import {Student} from './review';

export interface MentorApply {
  mentorId: number;
  mentorName: string;
  mentorSecondName: string;
  lessonType: string;
  timezone: string;
  availableDays: string[];
  availablePeriod: string[];
  student: Student;
}
