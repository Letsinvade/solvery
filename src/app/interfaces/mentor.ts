import {SocialLink} from './social-link';
import {Review} from './review';

export interface Mentor {
  id: number;
  avatar: string;
  sessionsCount: number;
  studentsCount: number;
  baseInfo: MentorBaseInfo;
  hireInfo: MentorHireInfo;

  canHelpList: string[];
  socialNetworkList?: SocialLink[];
  resume: MentorJob[];
  projectList?: Project[];
  educationList?: School[];
  publicationList?: Publication[];
  reviews?: Review[];
}

export interface MentorBaseInfo {
  name: string;
  secondName: string;
  country: string;
  city: string;
  timezone: string;
  descriptionShort: string;
  currentJob: MentorJob;
  tagList: string[];
  rating: number;
}

export interface MentorJob {
  positionShort?: string;
  positionLong?: string;
  companyName?: string;
  companyLink?: string;
  started?: string;
  finished?: string;
}

export interface Project {
  name: string;
  link?: string;
  description?: string;
}

export interface School {
  name: string;
  link?: string;
  description?: string;
}

export interface Publication {
  name: string;
  link?: string;
}

export interface MentorHireInfo {
  testCall: boolean;
  priceList: MentorPrice[];
}

export interface MentorPrice {
  lessonType: string;
  price: number;
  extraInfo?: string;
  timePeriod?: string;
}
