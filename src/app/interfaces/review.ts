export interface Review {
  author: Student;
  review: string;
  date: string;
}

export interface Student {
  name: string;
  secondName: string;
  phone?: string;
  mail?: string;
  avatar?: string;
  description?: string;
}
