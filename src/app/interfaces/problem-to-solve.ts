export interface ProblemToSolve {
  name: string;
  desc: string;
  key: string;
}
