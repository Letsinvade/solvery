export const enumContainer = {
  lessonType: {
    HALF_HOUR: 'HALF_HOUR',
    HOUR: 'HOUR',
    WEEK: 'WEEK',
    MONTH: 'MONTH',
    TEST: 'TEST'
  },
  availableDays: {
    MONDAY: 'MONDAY',
    TUESDAY: 'TUESDAY',
    WEDNESDAY: 'WEDNESDAY',
    THURSDAY: 'THURSDAY',
    FRIDAY: 'FRIDAY',
    SATURDAY: 'SATURDAY',
    SUNDAY: 'SUNDAY'
  },
  timePeriod: {
    EARLY_MORNING: 'EARLY_MORNING',
    MORNING_TO_MIDDAY: 'MORNING_TO_MIDDAY',
    MIDDAY_TO_EVENING: 'MIDDAY_TO_EVENING',
    LATER_EVENING: 'LATER_EVENING',
  },
  screenWidth: {
    TABLET_HORIZONTAL: 1023,
    TABLET: 768,
    DESKTOP: 1440
  }
};
