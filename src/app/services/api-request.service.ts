import {Injectable} from '@angular/core';
import {MentorApply} from '../interfaces/mentor-apply';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    })
};

@Injectable({
  providedIn: 'root'
})
export class ApiRequestService {
  private privateApiUrl: string;

  constructor(private http: HttpClient) {
    this.privateApiUrl = 'http://localhost:3000/api/';
  }


  postCookieRequest<T>(path: string, params?: any): Observable<any> {
    const requestParams = params || {};

    return this.handleErrors(this.http
        .post<any>(`${this.privateApiUrl}${path}`, requestParams, httpOptions),
      path);
  }


  applyMentorByMail(applyData: any): Observable<any> {
    return this.http.post( this.privateApiUrl + 'notification/mail', applyData);
  }

  applyMentorByTelegramm(applyData: any): Observable<any> {
    return this.http.post( this.privateApiUrl + 'telegram-api/message', applyData);
   // return this.postCookieRequest('telegram-api/message', applyData);
  }

  private handleErrors(observable: Observable<any>, path: string): Observable<any> {
    return observable.pipe(
      catchError((err: HttpErrorResponse): Observable<any> => {
        console.log('ERROR', err);
        return null;
      })
    );
  }
}
