import {Injectable} from '@angular/core';
import {Mentor} from '../interfaces/mentor';
import {MentorData} from '../data/mentorData';

@Injectable({
  providedIn: 'root'
})
export class MentorDataService {

  constructor() {
  }

  public getMentorList(): Mentor[] {
    return MentorData;
  }

  public getMentorById(id: number): Mentor {
    const searchResult = MentorData.find(mentor => mentor.id === id);
    if (searchResult) {
      return searchResult;
    }

    console.log(`Can't find mentor with this ID ${id}`);
    return null;
  }
}
