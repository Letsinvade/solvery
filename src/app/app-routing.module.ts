import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainPageComponent} from './pages/main-page/main-page.component';
import {MentorPageComponent} from './pages/mentor-page/mentor-page.component';
import {SearchPageComponent} from './pages/search-page/search-page.component';
import {BecomeMentorComponent} from './pages/become-mentor/become-mentor.component';

const routes: Routes = [
  {path: '', component: MainPageComponent},
  {path: 'mentor/:id', component: MentorPageComponent},
  {path: 'search', component: SearchPageComponent},
  {path: 'becomeMentor', component: BecomeMentorComponent},
  {path: 'pathForMentorPreview/:id',
    loadChildren: () => import('./modules/social-preview/social-preview.module').then(m => m.SocialPreviewModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
