import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SocialPreviewComponent} from './social-preview/social-preview.component';

const routes: Routes = [
  {
    path: '', component: SocialPreviewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SocialPreviewRoutingModule { }
