import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialPreviewRoutingModule } from './social-preview-routing.module';
import { SocialPreviewComponent } from './social-preview/social-preview.component';

@NgModule({
  imports: [
    CommonModule,
    SocialPreviewRoutingModule
  ],
  declarations: [SocialPreviewComponent]
})
export class SocialPreviewModule { }
