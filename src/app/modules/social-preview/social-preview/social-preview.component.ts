import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MentorDataService} from '../../../services/mentor-data.service';
import {Mentor} from '../../../interfaces/mentor';

@Component({
  selector: 'app-social-preview',
  templateUrl: './social-preview.component.html',
  styleUrls: ['./social-preview.component.scss']
})
export class SocialPreviewComponent implements OnInit {
  public id: number;
  public mentor: Mentor;
  public mentorAvatar: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private mentorDataService: MentorDataService) { }

  ngOnInit() {
    this.id = Number(this.route.snapshot.params.id);
    if (this.id && this.id > 0) {
      this.mentor = this.mentorDataService.getMentorById(this.id);
      this.mentorAvatar = this.mentor.avatar.substr(4);
    } else {
      this.router.navigateByUrl('/');
    }
  }

}
