import { Component, OnInit } from '@angular/core';
import {MentorDataService} from '../../services/mentor-data.service';
import {Mentor} from '../../interfaces/mentor';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {
  public mentorList: Mentor[];

  constructor(private mentorDataService: MentorDataService) { }

  ngOnInit() {
    this.mentorList = this.mentorDataService.getMentorList();
  }

}
