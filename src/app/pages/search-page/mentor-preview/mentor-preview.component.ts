import {Component, Input, OnInit} from '@angular/core';
import {Mentor} from '../../../interfaces/mentor';
import {MatDialog} from '@angular/material';
import {ApplyFormComponent} from '../../../components/modals/apply-form/apply-form.component';

@Component({
  selector: 'app-mentor-preview',
  templateUrl: './mentor-preview.component.html',
  styleUrls: ['./mentor-preview.component.scss']
})
export class MentorPreviewComponent implements OnInit {
  @Input() mentor: Mentor;
  public minPrice: number;

  constructor(private dialog: MatDialog) {
  }

  ngOnInit() {
    if (this.mentor.hireInfo.priceList.length) {
      const prices = this.mentor.hireInfo.priceList.map(item => item.price);
      this.minPrice = Math.min(...prices);
    } else {
      this.minPrice = 0;
    }
  }

  openApplyModal() {
    const dialogRef = this.dialog.open(ApplyFormComponent, {
      width: '800px',
      data: {
        mentor: this.mentor,
        lessonType: ''
      },
    });
  }
}
