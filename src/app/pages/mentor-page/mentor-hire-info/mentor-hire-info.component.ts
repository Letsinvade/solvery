import {Component, Input, OnInit} from '@angular/core';
import {Mentor} from '../../../interfaces/mentor';
import {MatDialog} from '@angular/material';
import {ApplyFormComponent} from '../../../components/modals/apply-form/apply-form.component';

@Component({
  selector: 'app-mentor-hire-info',
  templateUrl: './mentor-hire-info.component.html',
  styleUrls: ['./mentor-hire-info.component.scss']
})
export class MentorHireInfoComponent implements OnInit {
  @Input() mentor: Mentor;

  constructor(private dialog: MatDialog) {
  }

  ngOnInit() {
  }

  openApplyModal(lessonType: string) {
    const dialogRef = this.dialog.open(ApplyFormComponent, {
      width: '800px',
      data: {
        mentor: this.mentor,
        lessonType
      },
    });
  }
}
