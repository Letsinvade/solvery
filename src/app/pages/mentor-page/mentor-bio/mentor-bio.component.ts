import {Component, Input, OnInit} from '@angular/core';
import {Mentor} from '../../../interfaces/mentor';

@Component({
  selector: 'app-mentor-bio',
  templateUrl: './mentor-bio.component.html',
  styleUrls: ['./mentor-bio.component.scss']
})
export class MentorBioComponent implements OnInit {
  @Input() mentor: Mentor;

  constructor() { }

  ngOnInit() {
  }

}
