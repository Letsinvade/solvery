import {Component, HostListener, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {MentorDataService} from '../../services/mentor-data.service';
import {Mentor} from '../../interfaces/mentor';
import {enumContainer} from '../../services/enumContainer';
import {MatDialog} from '@angular/material';
import {ApplyFormComponent} from '../../components/modals/apply-form/apply-form.component';

@Component({
  selector: 'app-mentor-page',
  templateUrl: './mentor-page.component.html',
  styleUrls: ['./mentor-page.component.scss']
})
export class MentorPageComponent implements OnInit {
  public mentor: Mentor;
  private id: number;
  public isMobileScreen: boolean;

  constructor(private route: ActivatedRoute,
              private dialog: MatDialog,
              private router: Router,
              private mentorDataService: MentorDataService) {
  }

  ngOnInit() {
    this.id = Number(this.route.snapshot.params.id);
    if (this.id && this.id > 0) {
      this.mentor = this.mentorDataService.getMentorById(this.id);
    } else {
      this.router.navigateByUrl('/');
    }

    this.onResize();
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.isMobileScreen = window.innerWidth < enumContainer.screenWidth.TABLET;
  }

  applyMentor() {
    const dialogRef = this.dialog.open(ApplyFormComponent, {
      width: '800px',
      data: {
        mentor: this.mentor,
        lessonType: ''
      },
    });
  }
}

