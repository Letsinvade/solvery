import {Component, Input, OnInit} from '@angular/core';
import {Mentor} from '../../../interfaces/mentor';
import {MatDialog} from '@angular/material';
import {ApplyFormComponent} from '../../../components/modals/apply-form/apply-form.component';

@Component({
  selector: 'app-mentor-mobile-hero',
  templateUrl: './mentor-mobile-hero.component.html',
  styleUrls: ['./mentor-mobile-hero.component.scss']
})
export class MentorMobileHeroComponent implements OnInit {
  @Input() mentor: Mentor;

  constructor(private dialog: MatDialog) {
  }

  ngOnInit() {
  }

  applyMentor() {
    const dialogRef = this.dialog.open(ApplyFormComponent, {
      width: '800px',
      data: {
        mentor: this.mentor,
        lessonType: ''
      },
    });
  }
}
