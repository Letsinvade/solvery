import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-price-list',
  templateUrl: './price-list.component.html',
  styleUrls: ['./price-list.component.scss']
})
export class PriceListComponent implements OnInit {
  @Input() priceList: any[];
  @Input() withTestCall: boolean;
  @Output() onClick: EventEmitter<string>;
  public resources: any;

  constructor() {
    this.resources = {
      HALF_HOUR: 'Консультация 30 минут',
      HOUR: 'Часовое занятие',
      WEEK: 'Два занятия + unlim переписка',
      MONTH: 'Месяц наставничества',
      TEST: 'Тестовый созвон на 15 мин'
    };

    this.onClick = new EventEmitter();
  }

  ngOnInit() {
    console.log(this.priceList);
  }

  onPriceClick(price) {
    console.log(price);
    this.onClick.next(price);
  }
}
