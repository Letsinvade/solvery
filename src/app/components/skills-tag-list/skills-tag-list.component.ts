import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-skills-tag-list',
  templateUrl: './skills-tag-list.component.html',
  styleUrls: ['./skills-tag-list.component.scss']
})
export class SkillsTagListComponent implements OnInit {
  @Input() skillTags: string[] = [];

  constructor() { }

  ngOnInit() {
  }

}
