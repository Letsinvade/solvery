import {Component, Input, OnInit} from '@angular/core';
import {MentorBaseInfo} from '../../../interfaces/mentor';

@Component({
  selector: 'app-user-hero',
  templateUrl: './user-hero.component.html',
  styleUrls: ['./user-hero.component.scss']
})
export class UserHeroComponent implements OnInit {
  @Input() mentor: MentorBaseInfo;
  @Input() inPreview: boolean;

  constructor() {
  }

  ngOnInit() {
  }

}
