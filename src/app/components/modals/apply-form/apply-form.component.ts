import {Component, ElementRef, Inject, OnInit, Renderer2} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from '@angular/material';
import {MentorApply} from '../../../interfaces/mentor-apply';
import {ApplySuccessModalComponent} from '../apply-success-modal/apply-success-modal.component';
import {enumContainer} from '../../../services/enumContainer';
import {ApiRequestService} from '../../../services/api-request.service';

@Component({
  selector: 'app-apply-form',
  templateUrl: './apply-form.component.html',
  styleUrls: ['./apply-form.component.scss']
})
export class ApplyFormComponent implements OnInit {
  public mentorApply: MentorApply;
  public isEveryThingReady: boolean;
  public emailValid: boolean;
  public timezoneList: string[];
  public lessonTypes: any;

  constructor(private dialogRef: MatDialogRef<ApplyFormComponent>,
              public dialog: MatDialog,
              private el: ElementRef,
              private renderer: Renderer2,
              private apiRequest: ApiRequestService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.lessonTypes = enumContainer.lessonType;
    this.mentorApply = {
      mentorId: this.data.mentor.id,
      mentorName: this.data.mentor.baseInfo.name,
      mentorSecondName: this.data.mentor.baseInfo.secondName,
      lessonType: this.data.lessonType || '',
      timezone: 'UTC+3, Москва',
      availableDays: [],
      availablePeriod: [],
      student: {
        name: '',
        secondName: '',
        phone: '',
        mail: '',
        description: ''
      }
    };

    this.timezoneList = [
      'UTC+2, Калининград',
      'UTC+3, Москва',
      'UTC+4, Самара',
      'UTC+5, Екатеринбург',
      'UTC+6, Омск',
      'UTC+7, Красноярское',
      'UTC+8, Иркутск',
      'UTC+9, Якутск',
      'UTC+10, Владивосток',
      'UTC+11, Магадан',
      'UTC+12, Камчатск'
    ];
  }

  ngOnInit() {
    this.dialogRef.afterClosed().subscribe(result => {
      if (result === 'applied') {
        const successDialog = this.dialog.open(ApplySuccessModalComponent,
          {
            width: '600px',
            data: {
              studentName: this.mentorApply.student.name
            }
          });
      }
    });
  }

  closeModal() {
    this.dialogRef.close();
  }

  onLessonTypeChange(lessonType, applyForm) {
    this.mentorApply.lessonType = lessonType;
    this.checkIfFormReady(applyForm);
  }

  onDaysChange(availableDays, applyForm) {
    this.mentorApply.availableDays = availableDays;
    this.checkIfFormReady(applyForm);
  }

  onTimePeriodChange(availablePeriod, applyForm) {
    this.mentorApply.availablePeriod = availablePeriod;
    this.checkIfFormReady(applyForm);
  }

  checkIfFormReady(applyForm) {
    this.isEveryThingReady = !!(applyForm.valid
      && this.mentorApply.lessonType
      && this.mentorApply.availableDays.length
      && this.emailValid
      && this.mentorApply.timezone
      && this.mentorApply.availablePeriod.length);
  }

  onEmailChange(field, applyForm) {
    const re = /\S+@\S+\.\S+/;
    this.emailValid = re.test(field.value);
    if (this.emailValid) {
      this.checkIfFormReady(applyForm);
      this.renderer.removeClass(this.el.nativeElement.querySelector(`#${field.name}`), '-error');
    } else {
      this.renderer.addClass(this.el.nativeElement.querySelector(`#${field.name}`), '-error');
    }
  }

  onSubmit() {
    const data = {
      student: this.mentorApply.student,
      mentorApply: this.mentorApply
    };

    console.log(data);

    this.apiRequest.applyMentorByMail(data)
      .subscribe((response: any) => {
        console.log(response);
      });
    // this.dialogRef.close('applied');
  }
}
