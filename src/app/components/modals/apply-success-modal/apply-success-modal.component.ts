import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-apply-success-modal',
  templateUrl: './apply-success-modal.component.html',
  styleUrls: ['./apply-success-modal.component.scss']
})
export class ApplySuccessModalComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
              private dialogRef: MatDialogRef<ApplySuccessModalComponent>) { }

  ngOnInit() {
    setTimeout(() => this.closeModal(), 4000);
  }

  closeModal() {
    this.dialogRef.close();
  }
}
