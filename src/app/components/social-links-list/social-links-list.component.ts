import {Component, Input, OnInit} from '@angular/core';
import {SocialLink} from '../../interfaces/social-link';

@Component({
  selector: 'app-social-links-list',
  templateUrl: './social-links-list.component.html',
  styleUrls: ['./social-links-list.component.scss']
})
export class SocialLinksListComponent implements OnInit {
  @Input() socialLinks: SocialLink[];

  constructor() { }

  ngOnInit() {
  }

}
