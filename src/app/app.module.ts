import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainPageComponent } from './pages/main-page/main-page.component';
import { MentorPageComponent } from './pages/mentor-page/mentor-page.component';
import { MentorHireInfoComponent } from './pages/mentor-page/mentor-hire-info/mentor-hire-info.component';
import { SkillsTagListComponent } from './components/skills-tag-list/skills-tag-list.component';
import { UserImageComponent } from './components/user/user-image/user-image.component';
import { SocialLinksListComponent } from './components/social-links-list/social-links-list.component';
import { MentorBioComponent } from './pages/mentor-page/mentor-bio/mentor-bio.component';
import { MentorPhotoBlockComponent } from './pages/mentor-page/mentor-photo-block/mentor-photo-block.component';
import { RatingComponent } from './components/rating/rating.component';
import { PriceListComponent } from './components/price-list/price-list.component';
import { ReviewsComponent } from './components/reviews/reviews.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { MentorPreviewComponent } from './pages/search-page/mentor-preview/mentor-preview.component';
import { UserHeroComponent } from './components/user/user-hero/user-hero.component';
import { ApplyFormComponent } from './components/modals/apply-form/apply-form.component';
import { ApplySuccessModalComponent } from './components/modals/apply-success-modal/apply-success-modal.component';
import { BecomeMentorComponent } from './pages/become-mentor/become-mentor.component';

import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {NgxMaskModule} from 'ngx-mask';
import { MentorMobileHeroComponent } from './pages/mentor-page/mentor-mobile-hero/mentor-mobile-hero.component';
import { PreviewDescriptionPipe } from './services/preview-description.pipe';
import {ApiRequestService} from './services/api-request.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    MentorPageComponent,
    MentorHireInfoComponent,
    SkillsTagListComponent,
    UserImageComponent,
    SocialLinksListComponent,
    MentorBioComponent,
    MentorPhotoBlockComponent,
    RatingComponent,
    PriceListComponent,
    ReviewsComponent,
    HeaderComponent,
    FooterComponent,
    SearchPageComponent,
    MentorPreviewComponent,
    UserHeroComponent,
    ApplyFormComponent,
    ApplySuccessModalComponent,
    MentorMobileHeroComponent,
    BecomeMentorComponent,
    PreviewDescriptionPipe
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    NgxMaskModule.forRoot(),
    MatExpansionModule,
    HttpClientModule
  ],
  entryComponents: [
    ApplyFormComponent,
    ApplySuccessModalComponent
  ],
  providers: [ApiRequestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
