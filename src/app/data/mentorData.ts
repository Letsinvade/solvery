import {Mentor} from '../interfaces/mentor';
import {enumContainer} from '../services/enumContainer';

export const MentorData: Mentor[] = [
  {
    id: 1,
    avatar: '../../assets/img/mentor_birillo_nastya.png',
    sessionsCount: 4,
    studentsCount: 4,
    baseInfo: {
      name: 'Анастасия',
      secondName: 'Бирилло',
      country: 'Россия',
      city: 'Санкт-Петербург',
      timezone: 'GMT+3',
      descriptionShort: 'Занимаюсь улучшением существующих и разработкой новых алгоритмов ' +
        'по поиску зависимостей в базах данных. Параллельно преподаю программирование школьникам ' +
        'и занимаюсь продуктовой разработкой в стартапе.',
      currentJob: {
        positionShort: 'Исследователь',
        companyName: 'JetBrains Research',
        companyLink: 'https://research.jetbrains.org/ru/'
      },
      rating: 8.5,
      tagList: [
        'Базы данных',
        'PostgreSQL',
        'Алгоритмы',
        'Java',
        'Python3.7+',
        'C#',
        'Node.js',
        'Mongo DB',
        'Angular2+',
        'LaTeX'
      ]
    },
    hireInfo: {
      testCall: true,
      priceList: [
        {
          lessonType: enumContainer.lessonType.HALF_HOUR,
          price: 1000
        },
        {
          lessonType: enumContainer.lessonType.HOUR,
          price: 1800
        },
        {
          lessonType: enumContainer.lessonType.WEEK,
          price: 5500,
          timePeriod: 'week'
        }
      ]
    },
    canHelpList: [
      'Разработка приложений на Java, C#',
      'Обработка и визуализация данных на Python',
      'Проектировании архитектуры приложений',
      'Написание SQL запросов',
      'Front-end и back-end разработка приложений на MEAN-стеке (MongoDB, Express, Angular, Node.js)',
      'Работа с API через Python, Node.js',
      'Оформление статей и презентаций в LaTeX'
    ],
    socialNetworkList: [
      {
        name: 'github',
        url: 'https://github.com/nbirillo'
      },
      {
        name: 'vk',
        url: 'https://vk.com/id19101049'
      }
    ],
    resume: [
      {
        positionLong: 'Исследователь в Лаборатории информационных систем, ' +
          'занимаюсь исследованиями различных зависимостей в базах данных',
        companyName: 'JetBrains Research',
        companyLink: 'https://research.jetbrains.org/ru/',
        started: 'Апрель 2017'
      },
      {
        positionLong: 'Преподаватель C# для ребят 11-13 лет',
        companyName: 'Digital Banana',
        companyLink: 'https://digital-banana.ru/',
        started: 'Апрель 2016'
      },
      {
        positionLong: 'Front-end разработчик',
        companyName: '4 квадрата',
        started: 'Февраль 2016',
        finished: 'Ноябрь 2016'
      }
    ],
    projectList: [
      {
        name: 'Cerrado',
        description: 'Разработка мобильного приложения по типу инстаграмма, ' +
          'Flutter + MongoDB + Node.js + Express'
      },
      {
        name: 'Banana CRM',
        description: 'Разработка внутренней CRM системы для курсов, ' +
          'MongoDB, Express, Angular, Node.js'
      }
    ],
    educationList: [
      {
        name: 'Computer Science Center',
        description: 'Направления - Computer Science, Data Science, ' +
          'Software Engineering. Год окочания 2020'
      },
      {
        name: 'Магистратура СПБГУ',
        description: 'Математико-механический, Математическое обеспечение ' +
          'и администрирование информационных систем, год окончания - 2020'
      },
      {
        name: 'Бакалавриат СПБГУ',
        description: 'Математико-механический, Фундаментальная информатика ' +
          'и информационные технологии, год окончания - 2017'
      }
    ],
    publicationList: [
      {
        name: 'Anastasia Birillo and Nikita Bobrov. Smart Caching for Efficient Functional Dependency Discovery. ' +
          '23rd European Conference on Advances in Databases and Information Systems, ADBIS 2019',
      },
      {
        name: 'Development of the matrix-vector approach in algorithms of Probabilistic-Logic Inference in Algebraic Bayesian Networks, ' +
          'Indistinct systems, soft computing and intelligent technologies (NSMVIT-2017)',
      },
      {
        name: 'Nikita Bobrov, Anastasia Birillo, George Chernishev. A survey of database dependency concepts. ' +
          'Proceedings of the Second Conference on Software Engineering and Information Management.',
      },
      {
        name: 'Algebraic Bayesian Networks: Probabilistic-Logic Inference Algorithms and Storage Structures, ' +
          'Finnish-Russian University Cooperation in Telecommunications',
      },
      {
        name: 'Processing and visualization algorithms of Algebraic Bayesian Networks, Educational technologies and community',
      }
    ],
    reviews: [
      {
        author: {
          name: 'Оля',
          secondName: 'Ломакина',
          avatar: '../../assets/img/olya.png'
        },
        review: '“Классный препод, за 4 занятия успели все и даже больше. Обязательно пойду в нему еще Классный препод, за ' +
          '4 занятия успели все и даже больше. Обязательно пойду в нему еще Классный препод, за 4 занятия успели все и даже больше. ' +
          'Обязательно пойду в нему еще Классный препод, за 4 занятия успели все и даже больше. Обязательно пойду в нему еще Классный ' +
          'препод, за 4 занятия успели все и даже больше. Обязательно пойду в нему еще”',
        date: '29 ноября'
      },
      {
        author: {
          name: 'Настя',
          secondName: 'Бирилло',
          avatar: '../../assets/img/nastyash.png'
        },
        review: '“Классный препод, за 4 занятия успели все и даже больше. Обязательно пойду в нему еще”',
        date: '14 июня'
      },
      {
        author: {
          name: 'Настя',
          secondName: 'Бирилло',
          avatar: '../../assets/img/nastyash.png'
        },
        review: '“Классный препод, за 4 занятия успели все и даже больше. Обязательно пойду в нему еще”',
        date: '14 июня'
      }
    ]
  },
  {
    id: 2,
    avatar: '../../assets/img/mentor_jenya_moiseenko.png',
    sessionsCount: 0,
    studentsCount: 0,
    baseInfo: {
      name: 'Женя',
      secondName: 'Моисеенко',
      country: 'Россия',
      city: 'Санкт-Петербург',
      timezone: 'GMT+3',
      descriptionShort: 'Увлекаюсь теорией языков программирования и формальными методами. ' +
        'Искренне верю в функциональное программирование и системы типов :)',
      currentJob: {
        positionShort: 'Исследователь',
        companyName: 'JetBrains Research',
        companyLink: 'https://research.jetbrains.org/ru/'
      },
      rating: 7.5,
      tagList: [
        'Функциональное программирование',
        'Haskell',
        'Rust',
        'OCaml'
      ]
    },
    hireInfo: {
      testCall: true,
      priceList: [
        {
          lessonType: enumContainer.lessonType.HALF_HOUR,
          price: 1000
        },
        {
          lessonType: enumContainer.lessonType.HOUR,
          price: 2000
        },
        {
          lessonType: enumContainer.lessonType.WEEK,
          price: 4200,
          timePeriod: 'week'
        }
      ]
    },
    canHelpList: [
      'Функциональное программирование',
      'Программирование на Haskell',
      'Программирование на Rust',
      'Программирование на OCaml',
      'Программирование с зависимыми типами на Coq',
      'Логическое программирование (Prolog)'
    ],
    resume: [
      {
        positionLong: 'Исследователь',
        companyName: 'JetBrains Research',
        companyLink: 'https://research.jetbrains.org/ru/',
        started: 'Январь 2016'
      },
      {
        positionLong: 'Junior C++ Developer',
        companyName: 'Dr.Web',
        started: 'Сентябрь 2015',
        finished: 'Декабрь 2015'
      }
    ],
    projectList: [
      {
        name: 'OCaml eDSL',
        link: 'https://github.com/JetBrains-Research/OCanren/',
        description: 'Коммитил в репозиторий проекта JetBrains — OCaml eDSL для логического программирования.'
      },
      {
        name: 'Верификация моделей',
        description: 'Осенью 2018 года побывал на стажировке в институте Макса Планка в Германии, ' +
          'где начал работу над проектом (и продолжаю по настоящее время) ' +
          'по верификации слабых моделей памяти в системе доказательства теорем Coq.'
      }
    ],
    educationList: [
      {
        name: 'Аспирантура СПБГУ',
        description: 'Математико-механический, Математическое обеспечение ' +
          'и администрирование информационных систем, год окончания - 2022'
      },
      {
        name: 'Магистратура СПБГУ',
        description: 'Математико-механический, Математическое обеспечение ' +
          'и администрирование информационных систем, год окончания - 2018'
      },
      {
        name: 'Бакалавриат СПБГУ',
        description: 'Математико-механический, Фундаментальная информатика ' +
          'и информационные технологии, год окончания - 2016'
      }
    ],
    socialNetworkList: [],
    reviews: []
  },
  {
    id: 3,
    avatar: '../../assets/img/mentor_andrey_kochnev.png',
    sessionsCount: 0,
    studentsCount: 0,
    baseInfo: {
      name: 'Андрей',
      secondName: 'Кочнев',
      country: 'Россия',
      city: 'Санкт-Петербург',
      timezone: 'GMT+3',
      descriptionShort: 'В настоящий момент являюсь Unity разработчиком с опытом в мобильном геймдеве более 4-х лет. ' +
        'Ранее закончил Мат-Мех СПбГУ, порсле чего успел поработать в таких компаниях, ' +
        'как Joy Craft Games, Playkot и Playneta, собрав все лучшие практики по разработке игр от ' +
        'лидеров петербургского рынка. В настоящее время занимаюсь развитием своего проекта, оказанием ' +
        'проектных услуг по прототипированию и разработке игр, а также консультирую молодых разработчиков.',
      currentJob: {
        positionShort: 'Indie-разработчик, фрилансер'
      },
      rating: 6,
      tagList: [
        'Unity',
        'Gavedev',
        'HyperCasual',
        'Casual',
        'Midcore',
        'Indie',
        'C#'
      ]
    },
    hireInfo: {
      testCall: true,
      priceList: [
        {
          lessonType: enumContainer.lessonType.HALF_HOUR,
          price: 600
        },
        {
          lessonType: enumContainer.lessonType.HOUR,
          price: 1100
        },
        {
          lessonType: enumContainer.lessonType.WEEK,
          price: 3600,
          timePeriod: 'week'
        }
      ]
    },
    canHelpList: [
      'Помогу определиться с выбором ключевых шаблонов для архитектуры игры',
      'Посоветую отличные инструментов для облегчения разработки на Unity',
      'Определю «бутылочное горлышко» в производительности и помогу его поправить',
      'Сформирую пакет конкретных советов по улучшению кода игры (Code Review + рекомендации по рефакторингу)',
      'Дам рекомендации по менеджменту проекта/команды'
    ],
    resume: [
      {
        positionLong: 'Старший программист Unity',
        companyName: 'Playneta',
        companyLink: 'https://playneta.gg/',
        started: '2018',
        finished: '2019'
      },
      {
        positionLong: 'Программист Unity',
        companyName: 'Playkot',
        companyLink: 'https://playkot.com/',
        started: '2016',
        finished: '2018'
      },
      {
        positionLong: 'Программист Unity',
        companyName: 'Joy Craft Games',
        companyLink: 'http://joycraft-games.com/',
        started: '2015',
        finished: '2016'
      }
    ],
    projectList: [
      {
        name: 'Nitro Nation Stories',
        description: 'Больные гонки'
      },
      {
        name: 'Underneath',
        description: 'VR шутер'
      },
      {
        name: 'Age of Magic',
        description: 'Мобильный баттлер'
      },
      {
        name: 'Tilix',
        description: 'Мобильный HyperCasual'
      },
      {
        name: 'Kiss-Kiss',
        description: 'Социальный гейминг'
      },
      {
        name: 'Color Dunk 3D',
        description: 'Мобильный HyperCasual'
      }
    ],
    educationList: [
      {
        name: 'Волгоградский государственный университет',
        description: 'Факультет математики и информационных технологий, Прикладная математика и информатика. Бакалавриат 2008-2012'
      },
      {
        name: 'Санкт-Петербургский государственный университет',
        description: 'Мматематико-механический факультет, Механика. Магистратура и аспирантура 2012 - 2018'
      }
    ],
    socialNetworkList: [],
    publicationList: [],
    reviews: []
  },
  {
    id: 4,
    avatar: '../../assets/img/mentor_anton_volkov.png',
    sessionsCount: 0,
    studentsCount: 0,
    baseInfo: {
      name: 'Антон',
      secondName: 'Волков',
      country: 'Россия',
      city: 'Санкт-Петербург',
      timezone: 'GMT+3',
      descriptionShort: 'Почти 5 лет занимаюсь фронтенд разработкой. ' +
        'За это время получил довольно разносторонний опыт: от лендингов, до разработки своего стартапа. ' +
        'Помимо основной работы, где разрабатываю на AngularJS также занимаюсь преподаванием веба группам школьников, ' +
        'а прошлым летом вдвоем с коллегой написали курс по переходу на React для верстальщиков.',
      currentJob: {
        positionShort: 'Front-end разработчик',
        companyName: 'DSX Technologies',
        companyLink: 'https://dsxt.uk/'
      },
      rating: 6,
      tagList: [
        'JavaScript',
        'AngularJS',
        'Angular+',
        'TypeScript',
        'React',
        'Redux',
        'NgRx',
        'Pug',
        'SCSS',
        'Webpack',
        'Gulp',
        'HTML',
        'CSS',
        'Ubuntu',
        'Tilda'
      ]
    },
    hireInfo: {
      testCall: true,
      priceList: [
        {
          lessonType: enumContainer.lessonType.HALF_HOUR,
          price: 1000
        },
        {
          lessonType: enumContainer.lessonType.HOUR,
          price: 1800
        },
        {
          lessonType: enumContainer.lessonType.WEEK,
          price: 5000,
          timePeriod: 'week'
        }
      ]
    },
    canHelpList: [
      'Помогу верстальщику перейти на следующий этап и начать работать с фреймворками',
      'Разобраться, как развернуть проект на виртуалках',
      'Расскажу про особенности фреймворков',
      'Объясню простым языком про RxJS или Redux',
      'Помогу, если только хотите погрузиться в веб'
    ],
    resume: [
      {
        positionLong: 'Front-end разработчик',
        companyName: 'DSX Technologies',
        companyLink: 'https://dsxt.uk/',
        started: 'Январь 2016',
      },
      {
        positionLong: 'Front-end разработчик',
        companyName: 'InMyRoom',
        companyLink: 'https://www.inmyroom.ru/',
        started: 'Июль 2015',
        finished: 'Декабрь 2015'
      },
      {
        positionLong: 'Преподаватель и технический специалист',
        companyName: 'Digital Banana',
        companyLink: 'https://digital-banana.ru/',
        started: 'Декабрь 2014'
      }
    ],
    projectList: [
      {
        name: 'Digital Banana',
        link: 'https://digital-banana.ru/',
        description: 'Школа программирования для старшеклассников в Санкт-Петербурге'
      },
      {
        name: 'Banana CRM',
        description: 'Бэкофис для школы — принимал участие'
      },
      {
        name: 'DSX',
        link: 'https://dsx.uk',
        description: 'Реализаця большого количества фичей в проекте'
      },
      {
        name: 'Азбука ремонта МТС',
        link: 'https://www.inmyroom.ru/landing/mtsazbukaremonta/',
        description: 'Промо проект для МТС с калькулятором ремонтных работ в новой квартире'
      }
    ],
    socialNetworkList: [],
    educationList: [],
    publicationList: [],
    reviews: []
  },
  {
    id: 5,
    avatar: '../../assets/img/mentor_nadya_voskresenskaya.jpg',
    sessionsCount: 4,
    studentsCount: 4,
    baseInfo: {
      name: 'Надежда',
      secondName: 'Воскресенская',
      country: 'Россия',
      city: 'Санкт-Петербург',
      timezone: 'GMT+3',
      descriptionShort: 'Искренне верю в силу дизайна, его фундаментальные основы, которые помогают делать жизнь лучше.',
      currentJob: {
        positionShort: 'Сооснователь дизайн-студии',
        companyName: 'Densktudio',
        companyLink: 'https://denkstudio.com/'
      },
      rating: 8.5,
      tagList: [
        'UI',
        'UIX',
        'Логотип',
        'Фирменный стиль',
        'Типографика',
        'Композиция',
        'Мобильные приложения'
      ]
    },
    hireInfo: {
      testCall: true,
      priceList: [
        {
          lessonType: enumContainer.lessonType.HALF_HOUR,
          price: 600
        },
        {
          lessonType: enumContainer.lessonType.HOUR,
          price: 1700
        },
        {
          lessonType: enumContainer.lessonType.WEEK,
          price: 6000,
          timePeriod: 'week'
        }
      ]
    },
    canHelpList: [
      'Разработка процесса взаимодействия с пользователем',
      'Создание дизайн системы',
      'Помощь в процессе разработки концепции и финиша',
      'Помощь в процессе разработки идеи для фирменного стиля и композиции',
      'Советы по шрифтам в проекте',
      'Общее понимание принципов композиции в дизайне',
      'Вопросы по разработке дизайна для мобильного приложения'
    ],
    socialNetworkList: [
    ],
    resume: [
      {
        positionLong: 'Cofounder of design studio',
        companyName: 'Densktudio',
        companyLink: 'https://denkstudio.com',
        started: 'Апрель 2014'
      },
      {
        positionLong: 'Graphic Designer',
        companyName: 'HiQ MobilEyes',
        companyLink: 'https://www.hiq.se',
        started: 'Ноябрь 2012'
      },
    ],
    projectList: [
    ],
    educationList: [
      {
        name: 'CJM',
        description: 'Курсы CJM. Год окочания 2018'
      },
      {
        name: 'Магистратура СПБГУ',
        description: 'Факультет искусств, кафедра графического дизайна, ' +
          'год окончания - 2012'
      },
      {
        name: 'Бакалавриат СПБГУ',
        description: 'Факультет искусств, кафедра графического дизайна, ' +
          'год окончания - 2010'
      },
    ],
    publicationList: [
    ],
    reviews: []
  },
  {
    id: 6,
    avatar: '../../assets/img/mentor_makar_glavnar.jpeg',
    sessionsCount: 8,
    studentsCount: 2,
    baseInfo: {
      name: 'Макар',
      secondName: 'Главанарь',
      country: 'Россия',
      city: 'Санкт-Петербург',
      timezone: 'GMT+3',
      descriptionShort: 'Занимался играми потом перешел в веб разработку. Около года занимался бекендом ' +
        'после чего оказался в фронтенде. Люблю писать чистый код, запариваюсь по архитектуре. ' +
        'Топлю за MVC, не люблю IE :D',
      currentJob: {
        positionShort: 'Front-end разработчик',
        companyName: 'СТД "Петрович"',
        companyLink: 'https://petrovich.ru/'
      },
      rating: 8.5,
      tagList: [
        'JavaScript',
        'React',
        'Redux',
        'TypeScript',
        'RxJS',
        'HTML',
        'CSS',
        'SASS',
        'Jest',
        'Sinon.JS',
        'Enzyme',
        'Node.js',
        'Express',
        'MongoDB',
        'Figma',
        'Ubuntu',
        'Git'
      ]
    },
    hireInfo: {
      testCall: true,
      priceList: [
        {
          lessonType: enumContainer.lessonType.HALF_HOUR,
          price: 1000
        },
        {
          lessonType: enumContainer.lessonType.HOUR,
          price: 1800
        },
        {
          lessonType: enumContainer.lessonType.WEEK,
          price: 5000,
          timePeriod: 'week'
        }
      ]
    },
    canHelpList: [
      'Расскажу, как стать веб разработчиком',
      'Помогу перейти верстальщику на следующий этап и начать работать с фреймворками',
      'Объясню простым языком про RxJS или Redux и покажу на практике',
      'Расскажу, зачем фронтендеру нужна фигма, и как с ней работать',
      'Помогу написать бэкенд, с которым удобно работать на фронте'
    ],
    socialNetworkList: [],
    resume: [
      {
        positionLong: 'Frontend разработчик',
        companyName: 'СТД "Петрович"',
        companyLink: 'https://research.jetbrains.org/ru/',
        started: 'Июнь 2019'
      },
      {
        positionLong: 'Backend разработчик',
        companyName: 'Digital Banana',
        companyLink: 'https://digital-banana.ru',
        started: 'Июнь 2018',
        finished: 'Май 2019',
      }
    ],
    projectList: [
      {
        name: 'Brothers In Colors',
        description: 'Кооперативный платформер на Unity + C#'
      },
      {
        name: 'Cerrado',
        description: 'Разработка мобильного приложения по типу инстаграмма, ' +
          'Flutter + MongoDB + Node.js + Express'
      }
    ],
    educationList: [],
    publicationList: [],
    reviews: []
  },
  {
    id: 7,
    avatar: '../../assets/img/mentr_gavrilov_mihail.JPG',
    sessionsCount: 8,
    studentsCount: 2,
    baseInfo: {
      name: 'Михаил',
      secondName: 'Гаврилов',
      country: 'Россия',
      city: 'Санкт-Петербург',
      timezone: 'GMT+3',
      descriptionShort: '15 лет в IT, Активно инвестирую в стартапы последние 5 лет',
      currentJob: {
        positionShort: 'Советник в банке ',
        companyName: 'Санкт-Петербург"',
        companyLink: 'https://www.bspb.ru/'
      },
      rating: 8.5,
      tagList: [
        'Привлечение инвестиций',
        'Запуск пилота с корпорацией',
        'Поиск стартапов для инвесторов',
        'Тестирование гипотез',
        'Поиск партнеров',
        'Выход на западные рынки',
        'Оценка стоимости стартапа'
      ]
    },
    hireInfo: {
      testCall: true,
      priceList: [
        {
          lessonType: enumContainer.lessonType.HALF_HOUR,
          price: 6000
        },
        {
          lessonType: enumContainer.lessonType.HOUR,
          price: 12000
        },
        {
          lessonType: enumContainer.lessonType.WEEK,
          price: 24000,
          timePeriod: 'week'
        }
      ]
    },
    canHelpList: [
      'Могу помочь стартапам правильно сформулировать свою идею и бизнес модель',
      'Найти рынок и первых клиентов',
      'Запустить пилоты с крупнейшими Российскими корпорациями',
      'Подготовиться к раундам инвестирования и найти правильных инвесторов',
      'Помогу с выходом на западные рынки через акселераторы',
      'А также объясню на какую сумму инвестиций можно рассчитывать и почему',
      'Инвесторам помогу найти подходящие проекты и сформулировать инвестиционную стратегию'
    ],
    socialNetworkList: [],
    resume: [
      {
        positionLong: 'Советник в банке',
        companyName: 'Санкт-Петербург"',
        companyLink: 'https://www.bspb.ru/',
      },
      {
        positionLong: 'CEO',
        companyName: 'Lipt-Soft',
        companyLink: 'https://www.lipt-soft.ru/',
      }
    ],
    projectList: [
      {
        name: 'Финансист',
        description: 'Венчурный инвестор',
        link: 'https://fin-ctrl.ru/',
      },
      {
        name: 'Fantasy Invest',
        description: 'Венчурный инвестор',
        link: 'https://apps.apple.com/ru/app/fantasy-invest/id1479666326',
      },
      {
        name: 'WeeKing',
        description: 'Венчурный инвестор',
        link: 'http://www.weeking.club/',
      }
    ],
    educationList: [],
    publicationList: [],
    reviews: []
  },
  {
    id: 8,
    avatar: '../../assets/img/mentr_gavrilov_mihail.JPG',
    sessionsCount: 8,
    studentsCount: 2,
    baseInfo: {
      name: 'Иван',
      secondName: 'Серебренников',
      country: 'Россия',
      city: 'Санкт-Петербург',
      timezone: 'GMT+3',
      descriptionShort: 'Продуктовый лид-дизайнер, предприниматель. Топлю за умение думать и коммуницировать, ' +
        'высокоуровневое проектирование, приоритет целей над инструментами. В свободное время — верховая езда и ' +
        'разные прочие хобби.',
      currentJob: {
        positionShort: 'Chief UX Officer',
        companyName: 'DSX Technologies"',
        companyLink: 'https://dsx.uk/ru/'
      },
      rating: 8.5,
      tagList: [
        'IA',
        'CJM',
        'JTBD',
        'Userflow'
      ]
    },
    hireInfo: {
      testCall: true,
      priceList: [
      ]
    },
    canHelpList: [
      'Я помогаю дизайнерам достигать их целей',
      'Хочешь устрилоть в продуктовую компанию? Ок',
      'Сделать свой продукт, посчитать экономику? Ок',
      'Сделать крупный проект, который опасаешься не вывезти? Ок',
      'Перейти из заказной разработки в продуктовую? Научиться продуктовой аналитике? Ок'
    ],
    socialNetworkList: [],
    resume: [
      {
        positionLong: 'Chief UX Officer',
        companyName: 'DSX Technologies',
        companyLink: 'https://dsx.uk/ru/',
        started: 'Октябрь 2018'
      },
      {
        positionLong: 'Senior UX Designer',
        companyName: 'EPAM Systems',
        companyLink: 'https://www.epam.com/',
        started: 'Октябрь 2016',
        finished: 'Октябрь 2018',
      },
      {
        positionLong: 'Senior UX Designer',
        companyName: 'SEMrush',
        companyLink: 'https://www.semrush.com/',
        started: 'Март 2014',
        finished: 'Сентябрь 2016',
      },
      {
        positionLong: 'Lead UI Designer',
        companyName: 'DocsVision',
        companyLink: 'https://docsvision.com/',
        started: 'Июль 2012',
        finished: 'Март 2014',
      },
      {
        positionLong: 'Идеолог',
        companyName: 'Emagnat.ru',
        started: 'Август 2011',
        finished: 'Июнь 2013',
      },
      {
        positionLong: 'Project manager',
        companyName: 'Rekvizitka',
        started: 'Сентябрь 2010',
        finished: 'Май 2011',
      },
      {
        positionLong: 'art-director',
        companyName: 'smartface.ru',
        started: 'Март 2010',
        finished: 'Сентябрь 2010',
      }
],
    projectList: [],
    educationList: [],
    publicationList: [],
    reviews: []
  },
];
