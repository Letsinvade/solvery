const express = require("express");
const app = express();

const body_parser = require("body-parser");

app.use(body_parser.json({limit: '50mb'}));
app.use(body_parser.urlencoded({extended:true, limit:'50mb'}));

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

const dotenv = require("dotenv");
dotenv.config();

const injector = require("./injector");

injector.inject_routers(app, injector);

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});
