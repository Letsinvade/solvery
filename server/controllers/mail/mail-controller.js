const injector = require(require("path").dirname(require.main.filename) + "/injector");
const mailService = injector.inject_service('MailService');

let solveryTeam = process.env['SOLVERY_TEAM'];

const sendMessagesToSolveryTeam = async (message) => {
    if (!solveryTeam) {
        return false;
    }
    solveryTeam = solveryTeam.split(',');
    let countSent = 0;
    for (const email of solveryTeam) {
        try {
            await mailService.send_message(email, message);
            countSent++;
        } catch (e) {
            console.log(e);
        }
    }
    return countSent;
};

module.exports = {
    sendMessagesToSolveryTeam
};
