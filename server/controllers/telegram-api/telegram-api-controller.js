const axios = require("axios");
const querystring = require("querystring");

const apiUrl = "https://api.telegram.org/";
const TOKEN = process.env['TOKEN'] ? process.env['TOKEN'] : null;
const CHATID = process.env['CHATID'] ? process.env['CHATID'] : null;

let queryData = {
    botToken: "",
    chatId: "",
    text: ""
};

const sendMessage = async (message) => {
    try {
        queryData.botToken = TOKEN;
        queryData.chatId = CHATID;
        queryData.text = message;
        const response = await axios
            .post(apiUrl + `bot${queryData.botToken}/` + `sendMessage?${querystring.stringify({
                chat_id: queryData.chatId,
                text: queryData.text
            })}`);
        return response.data.ok;
    } catch (error) {
        // console.log(error);
        return null;
    }
};

module.exports = {
    sendMessage
};
