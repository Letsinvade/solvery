const newLineSymbol = '\n';
const separator = '______' + newLineSymbol;

const prepareTelegramMessage = (message) => {
    if (!message) {
        return '';
    }

    let result = '';
    result += `NEW REQUEST: ` + newLineSymbol;
    result += separator;

    result += `FROM: ${message.student.name} ${message.student.secondName}` + newLineSymbol;
    if (message.student.phone) {
        result += `phone: ${message.student.phone}`+ newLineSymbol
    }
    if (message.student.mail) {
        result += `mail: ${message.student.mail}`+ newLineSymbol
    }
    if (message.student.avatar) {
        result += `avatar: ${message.student.avatar}`+ newLineSymbol
    }
    if (message.student.description) {
        result += `avatar: ${message.student.description}`+ newLineSymbol
    }
    result += separator;

    result += `MENTOR: ${message.mentorApply.mentorId}` + newLineSymbol;
    result += `${message.mentorApply.mentorName} ${message.mentorApply.mentorSecondName}` + newLineSymbol;
    result += `Lesson type: ${message.mentorApply.lessonType}` + newLineSymbol;
    result += `Timezone: ${message.mentorApply.timezone}` + newLineSymbol;
    result += 'Available days: ';
    for (const day of message.mentorApply.availableDays) {
        result += day + '; ';
    }
    result += newLineSymbol;
    result += 'Available period: ';
    for (const time of message.mentorApply.availablePeriod) {
        result += time + '; ';
    }
    result += newLineSymbol;
    result += separator;
    result += separator;
    return result;
};

module.exports = {
    prepareTelegramMessage
};
