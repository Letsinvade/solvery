const nodemailer = require('nodemailer');

const MAIL = process.env['MAIL'];
const PASSWORD = process.env['PASSWORD'];

console.log('THIS IS MAIL ', MAIL);

const smtpTransport = require('nodemailer-smtp-transport');

const transporter = nodemailer.createTransport(smtpTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    requireTLS: true,
    auth: {
        user: MAIL,
        pass: PASSWORD
    }
}));

const send_message = async (email, context) => {
    const options = {
        from: MAIL,
        to: email,
        subject: 'NEW REQUEST',
        text: `${context}`
    };
    return await transporter.sendMail(options);
};

module.exports = {
    send_message
};
