module.exports = function (app, injector) {
    const mailController = injector.inject_controller('MailController');
    const notificationHelperController = injector.inject_controller('NotificationHelperController');

    app.route('/api/notification/mail')
        .post((req, res, next) => {
            mailController.sendMessagesToSolveryTeam(notificationHelperController
                .prepareTelegramMessage(req.body)).then(result => {
                    if (result) {
                        return res.json({
                            data: {
                                message: "SUCCESS FOR " + result + " EMAILS"
                            }
                        })
                    } else {
                        res.status(500);
                        return res.json({
                            is_error: true,
                            key_error: "INTERNAL_SERVER_ERROR"
                        });
                    }
            });
        });
};
