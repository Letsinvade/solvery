module.exports = function (app, injector) {
    const telegramApiController = injector.inject_controller('TelegramApiController');
    const notificationHelperController = injector.inject_controller('NotificationHelperController');

    app.route('/api/telegram-api/message')
        .post((req, res, next) => {
            telegramApiController.sendMessage(notificationHelperController
                .prepareTelegramMessage(req.body)).then(result => {
                if (result === null) {
                    res.status(500);
                    return res.json({
                        is_error: true,
                        key_error: "INTERNAL_SERVER_ERROR"
                    });
                }
                else if (!result){
                    res.status(500);
                    return res.json({
                        is_error: true,
                        key_error: "TELEGRAM_API_ERROR"
                    });
                }
                return res.json({
                    data: {
                        message: "SUCCESS"
                    }
                })
            });
        });
};
